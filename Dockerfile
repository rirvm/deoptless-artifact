FROM ubuntu:20.04
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8
# Install dependencies
RUN \
  apt-get update && \
  DEBIAN_FRONTEND=noninteractive apt-get upgrade -y -qq && \
  DEBIAN_FRONTEND=noninteractive apt-get install -y -qq curl git gcc gfortran g++ libreadline-dev libx11-dev libxt-dev zlib1g-dev libbz2-dev liblzma-dev libpcre3-dev libcurl4-openssl-dev libcairo2-dev make libreadline8 libncurses-dev xz-utils cmake tcl-dev tk-dev locales python3-pip sudo time rsync && \
  locale-gen en_US.UTF-8 && update-locale LANG=en_US.UTF-8 && \
  apt-get clean && rm -rf /var/cache/apt/lists
# Add benchmarks and benchmark infra
RUN \
  git clone --depth 1 https://github.com/smarr/ReBench.git /opt/ReBench && cd /opt/ReBench && \
  # See https://github.com/smarr/ReBench/issues/189
  sed -i 's/sudo /sudo -E /' rebench/executor.py && \
  pip3 install . && \
  git clone --depth 10 https://github.com/reactorlabs/rbenchmarking /opt/rbenchmarking && cd /opt/rbenchmarking && \
  git checkout a92447b37a03e96f8da1e18eb3cd8ab3b46fbf89
# Build the VM version 1adb6a (with a neccessary fixes from 6d319a and 90e180)
RUN \
  git clone  https://github.com/reactorlabs/rir /opt/rsh && \
  cd /opt/rsh && \
  git checkout 1adb6a2c1d96682dd71564d35de4351913ffb9ee && \
  git config user.email x && git config user.name x && \
  git merge 6d319a2b7e867b06e721c0fc908fdb0d7e7e0a12 -m x && \
  git cherry-pick 90e18068b73497699abe51765d12da01894dc2ed && \
  cmake -DCMAKE_BUILD_TYPE=release . && \
  make setup && \
  cmake . && \
  rm /opt/rsh/external/clang+llvm-12.0.0-x86_64-linux-gnu-ubuntu-20.04.tar.xz && \
  make -j8
# Install R packages for plotting
RUN \
  /opt/rsh/external/custom-r/bin/R -e \
    "install.packages(c('ggplot2','ggrepel'), repos='http://cran.r-project.org')"
# Add experiments for the paper
ADD experiments /opt/experiments/
ADD experiments.sh /opt/experiments.sh
# Run experiments
CMD /opt/experiments.sh
