## 0. Getting Started

This artifact uses a container based build process and contains a pre-built
image with the sources for the Ř virtual machine, including the patches for
the paper, the complete build environment and infrastructure for all experiments.

The artifact was tested with `docker 20.10.7` on `Linux 5.4.0` and
`podman 3.4.4` on `Linux 5.10.90`.

To test the basic functionality, the following script loads the pre-built
image and then runs the benchmark for Figure 4. The numbers should
roughly correspond to the levels seen in the graph.

```
docker load -i deoptless-artifact-image.tgz
docker run -e PIR_DEOPTLESS=0 deoptless-artifact \
  /opt/rsh/bin/Rscript /opt/experiments/litmus/litmus.r normal
docker run -e PIR_DEOPTLESS=1 deoptless-artifact \
  /opt/rsh/bin/Rscript /opt/experiments/litmus/litmus.r deoptless
```

### 0.1 Contents

All our contributions and dependencies are in the `/opt` directory
in the image:

```
ReBench:          Benchmark harness (external dependency)
experiments:      Experiments from the paper
experiments.sh:   Script to run all experiments
rbenchmarking:    Ř benchmark suite
rsh:              Ř virtual machine
rsh/rir:          Ř sources
rsh/external:     Ř external dependencies
rsh/bin:          Ř scripts
```

## 1. Build

The included `Dockerfile` provides all the necessary steps to build
the image of this artifact from online sources. This takes about
30 minutes and downloads several hundred Mb:

```
docker build -t deoptless-artifact .
```

The build script takes care of downloading the VM, including
patches for this paper, dependencies and benchmarks. Inspect the
`Dockerfile` for details.

Alternatively, this artifact comes with a pre-built image that was
produced by the above command. It can be loaded by:

```
docker load -i deoptless-artifact-image.tgz
```

## 2. Reproduce

This artifact includes a script `experiments.sh` to reproduce the
graphs from the paper. For each graph, the script:

1. Runs the corresponding benchmark without deoptless.
2. Runs the same benchmark with deoptless.
3. Invokes an R script to create the figure from the raw data and export all results.

It should be easy to follow this pattern in the steps by `experiments.sh`.

Some benchmarks are run using ReBench, which produces a `.data` file in
a tabular format. Others just print the measurements to the console.

The following command runs the whole script. This takes up to 5 hours to run:

```
mkdir results
docker run --privileged -v $(pwd)/results:/opt/results deoptless-artifact
```

After successful completion the local `results` directory should contain
the following items for each figure:

1. the baseline run times
2. the deoptless run times
3. a log of memory usage
3. the graph from the paper

To verify the claims of the paper simply compare each graph.

### 2.1 Limitations and errata

In general, the results vary depending on the machine and concurrent workloads.
To reduce noise all background tasks should be stopped. The `--privileged` option
allows the benchmark harness to set some noise-reducing settings using
`rebench-denoise`, though some will still fail due to the containerization
and there will be a warning (the problem is reported upstream).

Figure 4 is just for illustrative purposes. The benchmark is only run once
and the values don't have statistical significance. Labels were added manually.

In Figure 6, when packaging the artifact we noticed two issues. First, the
`convolution` benchmark had an instable baseline. Second, there are occasional
crashes or miscompilations with deoptless enabled. Thus, we included some
bugfix commits from later development. This changed the speedup for the
`convolution` benchmark. There are still occasional errors in the deoptless
mode in this benchmark that we could not resolve in time. They do not affect
the technique and are likely due to an implementation error, caused by
deoptless exercising less tested parts of Ř.

Figure 8 was produced from an interactive session which was not automated.

A previous version of this artifact failed to correctly set the environment
variables for controlling experiments in fig 6 and 11, producing incorrect
graphs.

## 3. Reuse

The patches introducing deoptless to the Ř VM were added upstream, behind
the feature flag `PIR_DEOPTLESS`. In the artifact image the sources for the
VM can be found in `/opt/rsh`. All dependencies are available to build it.
Simply type `make` in that directory.

The entrypoint for deoptless is the `deopt` builtin from the native backend,
in `rir/src/compiler/native/builtins.cpp`, starting at line 853. The normal
deopt starts after that at line 939. This corresponds to the simplified
pseudocode in Listing 6.

There is a `deoptlessDebug` flag that can be turned on. This causes
deoptless events to be logged to the console.

All experiments and the R scripts to plot the results can be found in
`/opt/experiments`. The benchmarks (which are just the default upstream Ř
benchmarks) are in `/opt/rbenchmarking`.
