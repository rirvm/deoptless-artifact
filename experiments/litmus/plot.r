require(ggplot2)
require(ggrepel)
d = read.csv("results.csv", na.strings=" ", header=F, col.names=c("experiment","event","type","run","s"))

scaleFUN <- function(x) sprintf("%.1f", x)
d$iteration = d$run
d$seconds = d$s
d$experiment = factor(d$experiment, c("normal", "deoptless"))
print(d[2,]$event)
p = ggplot(d, aes(iteration,seconds)) +
  theme_bw() +
  scale_colour_brewer(palette = "Dark2")+
  facet_wrap(~experiment, ncol=1, strip.position="left") +
  theme(
        #axis.title.x=element_blank(),
        #axis.text.x=element_blank(),
        #axis.title.y=element_blank(),
        #axis.text.y=element_blank(),
        axis.ticks=element_blank(),
        panel.grid.minor=element_blank(),
        panel.border = element_blank(),
        #panel.grid.major=element_blank(),
        legend.position = "none",
        #strip.background=element_blank(),
        strip.text=element_text(size=14),
        text=element_text(size=14)
        ) +
  scale_y_continuous(trans='log10', labels=scaleFUN) +
  geom_text_repel(aes(label = event,
                       color = experiment
                       ),
                       size = 6,
                       seed = 3,
                       nudge_y=0.4,
                       nudge_x=1,
                       show.legend=F) +
  stat_summary(aes(color=experiment), geom="point", size=4)

ggsave(p, filename = "litmus.pdf", height=4.5, device = cairo_pdf)
