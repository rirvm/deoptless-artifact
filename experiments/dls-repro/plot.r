source(file="../experiments-common.r") 

require(ggplot2)

data = rbind(
  readBMDataFile("deoptless", "/opt/experiments/dls-repro/benchmarks-deoptless.data"),
  readBMDataFile("baseline", "/opt/experiments/dls-repro/benchmarks-baseline.data")
)

printResult <- function(name, result, digits=3, space=F) {
   name = gsub("μ", "mu", name)
   r = format(result, digits=digits)
   cat(paste0("\\newcommand{\\result",name,"}{",r))
   if (space)
     cat("\\xspace")
   cat("}\n")
}


position_var_nudge <- function(l_off=0, r_off=0) {
  ggproto(NULL, PositionVarNudge, l_off=l_off, r_off=r_off)
}

PositionVarNudge <- ggproto("PositionVarNudge", Position,
  l_off = 0,
  r_off = 0,

  setup_params = function(self, data) {
    list(l_off = self$l_off, r_off = self$r_off)
  },

  compute_layer = function(self, data, params, layout) {
    nudge = data
    nudge$ord = 1:nrow(nudge)
    nudge$nudge = 0

    for (p in unique(data$PANEL)) {
      for (e in unique(data$x)) {
        n = nrow(nudge[nudge$x == e & nudge$PANEL == p,])
        if (n == 0) next()
        n_tot = n + params$l_off + params$r_off
        steps <- (0.9:(n_tot))/n_tot
        width <- 0.6
        steps <- steps * width - (width/2)
        d2 = nudge[nudge$x == e & nudge$PANEL == p,]
        d2 = d2[order(d2$iteration),]
        d2$nudge = -steps[(params$r_off+1):(n_tot-params$l_off)]
        for (d in d2$ord)
          nudge[nudge$ord == d,]$nudge = d2[d2$ord == d,]$nudge
       }
    }
    nudge = nudge$nudge
    f <- function(x) {
      x - nudge
    }
    transform_position(data, f, NULL)
  }
)


data = normalize(data, T, "baseline", T)
data = aggregate(list(speedup=data$speedup), by=list(benchmark=data$benchmark, iteration=data$iteration ), FUN=mean)
data$benchmark = substring(data$benchmark, 10)

p = ggplot(data, aes(benchmark, speedup, color=benchmark)) +
    theme_bw() +
    scale_colour_brewer(palette = "Dark2")+
    geom_point(position=position_var_nudge(),
               alpha=0.8,
               size=2,
               aes(iteration=iteration)
               ) +
    theme(axis.title.x=element_blank(),
          axis.ticks=element_blank(),
          panel.border = element_blank(),
          legend.position="none")

ggsave(p, filename = "plot.pdf", height=2, width=3, device = cairo_pdf)
