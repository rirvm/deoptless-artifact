require(ggplot2)
require(ggrepel)

getDataForExperimentLargeDataframe = function() {
    data1 <- read.csv("baseline-data.csv", na.strings=" ")
    data2 <- read.csv("deoptless-data.csv", na.strings=" ")
    rbind(data1, data2)
}

d = getDataForExperimentLargeDataframe()  


scaleFUN <- function(x) sprintf("%.2f", x)

d$iteration = d$run
d$time = d$s
d$benchmark = "dataframe"
d$experiment = factor(d$experiment, c("normal", "deoptless"))

p = ggplot(d, aes(iteration,time)) +
  theme_bw() +
  scale_colour_brewer(palette = "Dark2")+
  facet_wrap(~experiment, ncol=1, strip.position="left") +
  theme(
        axis.ticks=element_blank(),
        panel.grid.minor=element_blank(),
        panel.border = element_blank(),
        legend.position = "none",
        strip.text=element_text(size=14),
        text=element_text(size=14)
        ) +
  scale_y_continuous(trans='log10', labels=scaleFUN) +
  geom_text_repel(aes(label = event,
                       color = experiment
                       ),
                       size = 6,
                       seed = 3,
                       nudge_y=0.4,
                       nudge_x=1,
                       show.legend=F) +
    geom_point(size=2.5, aes(color=experiment), alpha=0.1)

ggsave(p, filename = "plot.pdf", height=4, device = cairo_pdf)
