source(file="../experiments-common.r") 

require(ggplot2)
require(ggrepel)

readFromFile <- function(experiment, benchmark) {
    file = paste0("volcano-", benchmark, "-", experiment, ".csv")
    res = read.csv(file, na.strings=" ", header=F, col.names=c("iteration", "time"))
    res$experiment = experiment
    res$benchmark = benchmark
    res
}

d =          readFromFile("baseline", "simplified")
d = rbind(d, readFromFile("deoptless", "simplified"))
d = rbind(d, readFromFile("baseline", "fun"))
d = rbind(d, readFromFile("deoptless", "fun"))
d = rbind(d, readFromFile("baseline", "type"))
d = rbind(d, readFromFile("deoptless", "type"))

d$time = as.numeric(d$time)

d = normalize(d, T, "baseline", T)
d$benchmark = factor(d$benchmark, c("simplified", "type", "fun"))
d$iteration = d$iteration-1

p = ggplot(d, aes(iteration,speedup)) +
  theme_bw() +
  scale_colour_brewer(palette = "Dark2")+
  facet_wrap(~benchmark, ncol=1, strip.position="left") +
  theme(
        axis.ticks=element_blank(),
        panel.grid.minor=element_blank(),
        panel.border = element_blank(),
        legend.position = "none",
        strip.text=element_text(size=14),
        text=element_text(size=14)
        ) +
    geom_hline(yintercept=1)+
  scale_y_continuous(trans='log10', labels=function(x) sprintf("%.1f", x)) +
  scale_x_continuous(breaks=c(0,5)) +
    geom_point(size=2.5, aes(color=benchmark), alpha=0.6)

ggsave(p, filename = "plot.pdf", height=4, width=5, device = cairo_pdf)
