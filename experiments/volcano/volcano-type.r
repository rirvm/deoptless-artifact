bench_rays <- function(height.map = volcano, sun.angle = 45) {
    height.map = height.map
    nc = ncol(height.map)
    nr = nrow(height.map)
    shadow <- matrix(1, ncol = nc, nrow = nr)
    sunangle <- sun.angle / 180 * pi
    angle <- -90 / 180 * pi
    diffangle <- 90 / 180 * pi
    numberangles <- 25
    anglebreaks <- sapply(seq(angle, diffangle, length.out = numberangles), tan)
    maxdistance <- floor(sqrt(nc^2 + nr^2))
    sinsun <- sin(sunangle)
    cossun <- cos(sunangle)

    for (i in 1:nr) {
        for (j in 1:nc) {
            for (anglei in anglebreaks) {
                for (k in 1:maxdistance) {
                    xcoord <- i + sinsun * k
                    ycoord <- j + cossun * k

                    if (xcoord > nr ||
                        ycoord > nc ||
                        xcoord < 0 || ycoord < 0) break

                    tanangheight <- height.map[i, j] + anglei * k

                    if (all(c(height.map[ceiling(xcoord), ceiling(ycoord)],
                              height.map[floor(xcoord),   ceiling(ycoord)],
                              height.map[ceiling(xcoord), floor(ycoord)],
                              height.map[floor(xcoord),   floor(ycoord)]) < tanangheight)) next

                    if (tanangheight < bilinear(height.map, xcoord, ycoord)) {
                        shadow[i, j] <- shadow[i, j] - 1 / length(anglebreaks)
                        break
                    }
                }
            }
        }
    }

    shadow
}

bilinear <- function(data, x0, y0) {
    i <- max(1, floor(x0))
    j <- max(1, floor(y0))
    XT <- (x0 - i)
    YT <- (y0 - j)
    result <- (1 - YT) * (1 - XT) * data[i, j]
    nx <- nrow(data)
    ny <- ncol(data)
    if (i + 1 <= nx) {
        result <- result + (1 - YT) * XT * data[i + 1, j]
    }
    if (j + 1 <= ny) {
        result <- result + YT * (1 - XT) * data[i, j + 1]
    }
    if (i + 1 <= nx && j + 1 <= ny) {
        result <- result + YT * XT * data[i + 1, j + 1]
    }
    result
}

points <- rep(181L, 10) # 181 takes the longest to compute

execute <- function() {
  for (i in points) {
    bench_rays(volcano, i)
  }
}

# ================== 1 ================================

cat("1,",system.time(execute())[[1]],"\n")
cat("2,",system.time(execute())[[1]],"\n")
cat("3,",system.time(execute())[[1]],"\n")
cat("4,",system.time(execute())[[1]],"\n")
cat("5,",system.time(execute())[[1]],"\n")

# ================== 2 ================================

a = attributes(volcano)
volcano = as.integer(volcano)
attributes(volcano) = a

cat("6,",system.time(execute())[[1]],"\n")
cat("7,",system.time(execute())[[1]],"\n")
cat("8,",system.time(execute())[[1]],"\n")
cat("9,",system.time(execute())[[1]],"\n")
cat("10,",system.time(execute())[[1]],"\n")
