bmselection <- c(
  "Bounce_nonames",
  "Mandelbrot",
  "Storage",
  "convolution",
  "flexclust",
  "binarytrees",
  "fannkuchredux",
  "fasta_naive_2",
  "fastaredux",
  "knucleotide",
  "nbody",
  "nbody_naive",
  "pidigits",
  "regexdna",
  "reversecomplement_naive",
  "spectralnorm_math"
  )


readBMDataFile <- function(name, file) {
  res <- read.table(file, header=F, strip.white=TRUE, col.names=c("execution", "iteration", "time", "time_unit", "ua", "benchmark",     "vm", "suite", "num_iterations", "ub"))
  res$experiment = name
  res
}

normalize <- function(data, removeNormalized, baseline, perIteration) {
  data$speedup = 1
  for (b in unique(data$benchmark)) {
    if (missing(baseline))
      e1 = unique(data$experiment)[1]
    else
      e1 = baseline
    m = mean(data[data$benchmark == b & data$experiment == e1, ]$time)
    data[data$benchmark == b, ]$speedup <-
      (if (perIteration) data[data$benchmark == b & data$experiment == e1, ]$time else m) / data[data$benchmark == b, ]$time
    if (removeNormalized)
      data = data[-which(data$benchmark == b & data$experiment == e1), ]
  }
  data
}
