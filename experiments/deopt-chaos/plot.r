source(file="../experiments-common.r") 

require(ggplot2)

warmup=5

data = rbind(
  readBMDataFile("deoptless", "benchmarks-deoptless.data"),
  readBMDataFile("baseline",  "benchmarks-baseline.data")
)
data = data[which(data$iteration > warmup), ]
data = data[data$benchmark %in% bmselection,]

data = normalize(data, T, "baseline", F)
data = aggregate(list(speedup=data$speedup), by=list(benchmark=data$benchmark, iteration=data$iteration ), FUN=mean)

position_var_nudge <- function(l_off=0, r_off=0) {
  ggproto(NULL, PositionVarNudge, l_off=l_off, r_off=r_off)
}

PositionVarNudge <- ggproto("PositionVarNudge", Position,
  l_off = 0,
  r_off = 0,

  setup_params = function(self, data) {
    list(l_off = self$l_off, r_off = self$r_off)
  },

  compute_layer = function(self, data, params, layout) {
    nudge = data
    nudge$ord = 1:nrow(nudge)
    nudge$nudge = 0

    for (p in unique(data$PANEL)) {
      for (e in unique(data$x)) {
        n = nrow(nudge[nudge$x == e & nudge$PANEL == p,])
        if (n == 0) next()
        n_tot = n + params$l_off + params$r_off
        steps <- (0.9:(n_tot))/n_tot
        width <- 0.6
        steps <- steps * width - (width/2)
        d2 = nudge[nudge$x == e & nudge$PANEL == p,]
        d2 = d2[order(d2$iteration),]
        d2$nudge = -steps[(params$r_off+1):(n_tot-params$l_off)]
        for (d in d2$ord)
          nudge[nudge$ord == d,]$nudge = d2[d2$ord == d,]$nudge
       }
    }
    nudge = nudge$nudge
    f <- function(x) {
      x - nudge
    }
    transform_position(data, f, NULL)
  }
)

plot = function(name, breaks) {
  cairo_pdf(name)
  print(ggplot(data, aes(benchmark, speedup, color=benchmark)) +
      theme_bw() +
      stat_summary(fun = mean, geom = "point", size = 5, alpha=0.5) +
      (if (!missing(breaks))
        scale_y_continuous(trans='log10', breaks=breaks)
      else scale_y_continuous(trans='log10'))+
      geom_point(position=position_var_nudge(),
                 size=0.5,
                 aes(iteration=iteration)
                 ) +
      geom_hline(yintercept=1)+
      theme(axis.title.x=element_blank(),
            axis.text.x=element_text(angle = 90, vjust = 0.5, hjust=1),
            axis.ticks=element_blank(),
            panel.border = element_blank(),
            legend.position="none") #+
  )
  dev.off()
}

stats = function() {
  means = aggregate(data[3], list(data$benchmark), mean)
  print(means)
  cat("median ")
  print(median(means$speedup))
  cat("min ")
  print(min(means$speedup))
  cat("max ")
  print(max(means$speedup))
}

plot("plot.pdf")
stats()
