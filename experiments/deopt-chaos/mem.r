normal = read.table("fig6-baseline.log", header=F, sep=",", col.names=c("name","time","mrss"))
deoptless = read.table("fig6-deoptless.log", header=F, sep=",", col.names=c("name","time","mrss"))

mem = numeric()
for (n in unique(normal$name)) {
  name = strsplit(n, " ")[[1]][3]
  mem[name] = mean(deoptless[deoptless$name == n,]$mrss) / mean(normal[normal$name == n,]$mrss)
}

print(mem)
cat("max ")
print(max(mem))
cat("min ")
print(min(mem))
cat("median ")
print(median(mem))
