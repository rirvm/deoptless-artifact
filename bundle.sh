git clean -f -X
docker build -t deoptless-artifact .
docker save -o deoptless-artifact-image.tgz deoptless-artifact
cd ..
tar --exclude-vcs -czf deoptless-artifact.tgz deoptless-artifact
