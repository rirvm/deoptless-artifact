#!/bin/bash

mkdir -p /opt/results
# in case this script is run multiple times...
rm -f /opt/results/*


echo "**** Fig 4: Run BM 1/2"
PIR_DEOPTLESS=0 /usr/bin/time -a -o /opt/results/fig4.log -f '%C,%E,%M' /opt/rsh/bin/Rscript /opt/experiments/litmus/litmus.r normal > /opt/experiments/litmus/results.csv
echo "**** Fig 4: Run BM 2/2"
PIR_DEOPTLESS=1 /usr/bin/time -a -o /opt/results/fig4.log -f '%C,%E,%M' /opt/rsh/bin/Rscript /opt/experiments/litmus/litmus.r deoptless >> /opt/experiments/litmus/results.csv

echo "**** Fig 4: Plot"
cd /opt/experiments/litmus
/opt/rsh/external/custom-r/bin/Rscript plot.r
mv litmus.pdf /opt/results/fig4.pdf


echo "**** Fig 6: Run BM 1/2"
PIR_DEOPT_CHAOS=100000 PIR_DEOPT_CHAOS_NO_RETRIGGER=1 PIR_DEOPTLESS=0 /opt/rbenchmarking/Setup/run.sh /opt/experiments/deopt-chaos/rebench.conf /opt/rbenchmarking/Benchmarks /opt/rsh "e:PIR-LLVM -df /opt/experiments/deopt-chaos/benchmarks-baseline.data -R"
mv /tmp/memory.data /opt/results/fig6-baseline.log
echo "**** Fig 6: Run BM 2/2"
PIR_DEOPT_CHAOS=100000 PIR_DEOPT_CHAOS_NO_RETRIGGER=1 PIR_DEOPTLESS=1 /opt/rbenchmarking/Setup/run.sh /opt/experiments/deopt-chaos/rebench.conf /opt/rbenchmarking/Benchmarks /opt/rsh "e:PIR-LLVM -df /opt/experiments/deopt-chaos/benchmarks-deoptless.data -R"
mv /tmp/memory.data /opt/results/fig6-deoptless.log

echo "**** Fig 6: Plot"
cd /opt/experiments/deopt-chaos
/opt/rsh/external/custom-r/bin/Rscript plot.r
mv benchmarks-baseline.data /opt/results/fig6-benchmarks-baseline.data
mv benchmarks-deoptless.data /opt/results/fig6-benchmarks-deoptless.data
mv plot.pdf /opt/results/fig6.pdf
mv plot2.pdf /opt/results/fig6-2.pdf

echo "**** Fig 6: Memory Statistics"
cd /opt/results
/opt/rsh/external/custom-r/bin/Rscript /opt/experiments/deopt-chaos/mem.r


echo "**** Fig 9: Run BM 1/2"
cd /opt/experiments/volcano
for i in {1..3}; do
  PIR_DEOPTLESS=0 /opt/rsh/bin/Rscript /opt/experiments/volcano/volcano-fun.r        >> volcano-fun-baseline.csv
  PIR_DEOPTLESS=0 /opt/rsh/bin/Rscript /opt/experiments/volcano/volcano-type.r       >> volcano-type-baseline.csv
  PIR_DEOPTLESS=0 /opt/rsh/bin/Rscript /opt/experiments/volcano/volcano-simplified.r >> volcano-simplified-baseline.csv
done
echo "**** Fig 9: Run BM 2/2"
for i in {1..3}; do
  PIR_DEOPTLESS=1 /opt/rsh/bin/Rscript /opt/experiments/volcano/volcano-fun.r        >> volcano-fun-deoptless.csv
  PIR_DEOPTLESS=1 /opt/rsh/bin/Rscript /opt/experiments/volcano/volcano-type.r       >> volcano-type-deoptless.csv
  PIR_DEOPTLESS=1 /opt/rsh/bin/Rscript /opt/experiments/volcano/volcano-simplified.r >> volcano-simplified-deoptless.csv
done

echo "**** Fig 9: Plot"
/opt/rsh/external/custom-r/bin/Rscript plot.r
mv /opt/experiments/volcano/*.csv /opt/results/
mv plot.pdf /opt/results/fig9.pdf


echo "**** Fig 10"
cd /opt/experiments/bigdataframe
cp header "baseline-data.csv"
cp header "deoptless-data.csv"
echo "**** Fig 10: Run BM 1/2"
for i in {1..10}; do PIR_DEOPTLESS=0 /opt/rsh/bin/R -f bigdataframe.r --args "baseline-data.csv" "normal" 1> /dev/null ; done
echo "**** Fig 10: Run BM 2/2"
for i in {1..10}; do PIR_DEOPTLESS=1 /opt/rsh/bin/R -f bigdataframe.r --args "deoptless-data.csv" "deoptless" 1> /dev/null; done

echo "**** Fig 10: Plot"
cd /opt/experiments/bigdataframe
/opt/rsh/external/custom-r/bin/Rscript plot.r
mv baseline-data.csv /opt/results/fig10-baseline-data.csv
mv deoptless-data.csv /opt/results/fig10-deoptless-data.csv
mv plot.pdf /opt/results/fig10.pdf


echo "**** Fig 11: Run BM 1/2"
PIR_DEOPTLESS=0 /opt/rbenchmarking/Setup/run.sh /opt/experiments/dls-repro/rebench.conf /opt/rbenchmarking/Benchmarks /opt/rsh "e:PIR-LLVM -df /opt/experiments/dls-repro/benchmarks-baseline.data -R"
cp /tmp/memory.data /opt/results/fig11-baseline.log
echo "**** Fig 11: Run BM 2/2"
PIR_DEOPTLESS=1 /opt/rbenchmarking/Setup/run.sh /opt/experiments/dls-repro/rebench.conf /opt/rbenchmarking/Benchmarks /opt/rsh "e:PIR-LLVM -df /opt/experiments/dls-repro/benchmarks-deoptless.data -R"
cp /tmp/memory.data /opt/results/fig11-deoptless.log

echo "**** Fig 11: Plot"
cd /opt/experiments/dls-repro
/opt/rsh/external/custom-r/bin/Rscript plot.r
mv benchmarks-baseline.data /opt/results/fig11-benchmarks-baseline.data
mv benchmarks-deoptless.data /opt/results/fig11-benchmarks-deoptless.data
mv plot.pdf /opt/results/fig11.pdf
